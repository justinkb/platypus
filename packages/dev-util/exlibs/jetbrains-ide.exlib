# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

# Exlib for JetBrains Intellij Platform based products

# Required artifacts
#
# Distribution parts (as for for product-edition)
#   bin/product.sh - installed as /usr/bin/product-edition
#   bin/product.png - icon for desktop file
# Files:
#   $FILES/$PN.desktop - desktop file to run product from /usr/bin

require gtk-icon-cache

export_exlib_phases pkg_setup src_prepare src_install

MYOPTIONS="
    ( providers: jetbrains-jre system-jre ) [[
        *description = [ Boot JDK provider ]
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    run:
        dev-libs/glib:2
        providers:jetbrains-jre? ( dev-java/jetbrains-runtime )
        providers:system-jre? ( virtual/jre:* )
"

# These files should be executable
MY_LAUNCHER="bin/${PN%-*}.sh"
JETBRAINS_EXECUTABLES=(
    bin/format.sh
    bin/fsnotifier
    bin/fsnotifier64
    bin/inspect.sh
    bin/printenv.py
    bin/restart.py
)

jetbrains-ide_pkg_setup() {
    exdirectory --allow /opt
}

jetbrains-ide_src_prepare() {
    default
    nonfatal edo rm -r jbr # provided by jetbrains-runtime
    # Wrong architecture
    nonfatal edo rm -r \
        bin/fsnotifier-arm \
        lib/pty4j-native/linux/ppc64le

    # Use JBR automatically
    if optionq "providers:jetbrains-jre"; then
        edo sed "1a JAVA_HOME=/opt/jetbrains-runtime" \
            -i "${MY_LAUNCHER}"
    fi
}

jetbrains-ide_src_install() {
    local dest="/opt/${PN}"

    insinto "${dest}"
    doins -r ./*

    for exe in "${MY_LAUNCHER}" "${JETBRAINS_EXECUTABLES[@]}"; do
        edo chmod a+x "${IMAGE}/${dest}/${exe}"
    done

    dodir "/usr/$(exhost --target)/bin"
    dosym "${dest}/${MY_LAUNCHER}" "/usr/$(exhost --target)/bin/${PN}"

    insinto /usr/share/icons/hicolor/128x128/apps
    newins "bin/${PN%-*}.png" "${PN}.png"

    insinto "/usr/share/applications"
    doins "${FILES}/${PN}.desktop"
}

