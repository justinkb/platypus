# Copyright 2019 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_setup src_unpack src_prepare src_install pkg_postinst pkg_postrm

SUMMARY="Chromium based web browser from Yandex"
HOMEPAGE="https://browser.yandex.com"
DOWNLOADS="
    listed-only:
        https://repo.yandex.ru/${PN%-*}/deb/pool/main/y/${PN}/${PN}_${PV}-1_amd64.deb
"

LICENCES="yandex-browser-eula"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-arch/xz
"

UPSTREAM_RELEASE_NOTES="https://browser.yandex.ru/blog/"

WORK="${WORKBASE}"

yandex-browser_pkg_setup() {
    exdirectory --allow /opt
}

yandex-browser_src_unpack() {
    default
    edo tar xf data.tar.xz
}

yandex-browser_src_prepare() {
    default
    edo rm -r usr/share/menu  # Debian specific thing
    edo sed "/Exec/ s|/usr/bin/||" \
        -i "usr/share/applications/${PN}.desktop"
}

yandex-browser_src_install() {
    local yab_home="opt/${PN/-//}"  # opt/yandex/PN

    for s in 16 22 24 32 48 64 128 256 512; do
        insinto /usr/share/icons/hicolor/${s}x${x}/apps
        newins "${yab_home}/product_logo_${s}.png" "${PN}.png"
    done

    # TODO easier to do, but breaks $WORK
    edo mv opt "${IMAGE}"
    edo mv usr/share/* "${IMAGE}/usr/share"

    dodir /usr/$(exhost --target)/bin
    dosym "/${yab_home}/${PN}" "/usr/$(exhost --target)/bin/${PN}"
}

yandex-browser_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

yandex-browser_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

