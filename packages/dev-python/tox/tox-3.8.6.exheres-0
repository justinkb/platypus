# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="virtualenv-based automation of test activities"
DESCRIPTION="tox is a generic virtualenv management and test command line tool you can use for:
- checking your package installs correctly with different Python versions and interpreters
- running your tests in each of the environments, configuring your test tool of choice
- acting as a frontend to Continuous Integration servers,
greatly reducing boilerplate and merging CI and shell-based testing.
"
HOMEPAGE="https://tox.readthedocs.org/"

LICENCES="MIT"
PLATFORMS="~amd64"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm[python_abis:*(-)?]
    build+run:
        dev-python/filelock[>=3.0.0&<4][python_abis:*(-)?]
        dev-python/pluggy[>=0.3.0&<1.0][python_abis:*(-)?]
        dev-python/py[>=1.4.17&<2][python_abis:*(-)?]
        dev-python/six[>=1.0.0&<2][python_abis:*(-)?]
        dev-python/toml[>=0.9.4][python_abis:*(-)?]
        dev-python/virtualenv[>=1.11.2][python_abis:*(-)?]
    test:
        dev-python/freezegun[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
        dev-python/pytest-randomly[python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
        dev-python/pytest-xdist[python_abis:*(-)?]
"

# Requires unpackaged pytest-randomly which requires another load of unpackaged.
RESTRICT="test"

